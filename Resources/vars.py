gmm_capability = {
    "IEI": "10",
    "Length": "01",
    "S1Mode": "0",
    "HOattach": "0",
    "LPP": "0",
    "RestrictEC": "0",
    "5G-CPCIoT": "1",
    "N3data": "1",
    "5G-IPHC-CPCIoT": "0",
    "SGC": "0",
}

mobile_identity_suci = {
    "Type": "suci",
    "MNC digit 2": "0000",
    "MNC digit 1": "0011",
    "MCC digit 3": "0000",
    "MNC digit 3": "0010",
    "MCC digit 2": "0100",
    "MCC digit 1": "0110",
    "Routing indicator digit 2": "0000",
    "Routing indicator digit 1": "1111",
    "Routing indicator digit 4": "1111",
    "Routing indicator digit 3": "1111",
    "Protection scheme Id": "0000",
    "Home network public key": "00000000",
    "Scheme output": ["00000000", "00000000", "11110001"]
}

initial_reg_msg = {
    "command": "reg",
    "5GS registration type": "001",
    "5GMM capability": gmm_capability,
    "Mobile identity type": mobile_identity_suci
}

