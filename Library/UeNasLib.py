from NasConnectionHandler import NasConnectionHandler
import json


class UeNasLib:
    """
    Library class for UE-NAS-LIB. Keywords implemented here drive action from NAS stack
    """
    connection = None

    @classmethod
    def connect_ue_nas_socket(cls):
        """
        Keyword to create socket connection between UPTT to UE-NAS-Stack
        :return: None
        """
        if not cls.connection:
            nas_socket = NasConnectionHandler().connect_sock()
            cls.connection = nas_socket
        else:
            print('There is already a connection')

    @classmethod
    def disconnect_ue_nas_socket(cls):
        """
        Keyword to disconnect socket connection between UPTT to UE-NAS-Stack
        :return: None
        """
        if cls.connection:
            cls.connection.close()
            cls.connection = None

    @classmethod
    def send_registration_request(cls, msg):
        """
        Keyword for sending registration request from UE-NAS-Stack
        :return: None
        """
        if cls.connection:
            reg_msg = json.dumps(msg)
            cls.connection.sendall(bytes(reg_msg, encoding='utf-8'))
            print(f'Sent registration request: {reg_msg}')
            # data = cls.connection.recv(4096).decode('utf-8')
            while True:
                data = cls.connection.recv(4096).decode('utf-8')
                print(f'Response from UE-NAS: {data}')
                if data.strip() == 'quit':
                    break
            # cls.connection.sendall(bytes('', encoding='utf-8'))
        else:
            raise ConnectionError('Socket connection not up')

    @classmethod
    def send_de_registration_request(cls, msg):
        """
        Keyword for sending de-registration request from UE-NAS-Stack
        :return: None
        """
        if cls.connection:
            reg_msg = json.dumps(msg)
            cls.connection.sendall(bytes(reg_msg, encoding='utf-8'))
            print(f'Sent de-registration request: {reg_msg}')
            while True:
                data = cls.connection.recv(4096).decode('utf-8')
                print(f'Response from UE-NAS: {data}')
                if data.strip() == 'quit':
                    break
            # cls.connection.sendall(bytes('', encoding='utf-8'))
        else:
            raise ConnectionError('Socket connection not up')

        # if self.connection:
        #     self.connection.sendall(bytes(msg, encoding='utf-8'))
        #     print(f'Sent de-registration request: {msg}')
        #     data = self.connection.recv(4096).decode('utf-8')
        #     print(f'Response from UE-NAS: {data}')
        # else:
        #     raise ConnectionError('Socket connection not up')
