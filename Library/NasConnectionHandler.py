from pathlib import Path
import configparser
import socket


class NasConnectionHandler:
    """
    Connection handler class for managing connection between UPTT and UE-NAS-Stack
    """

    @staticmethod
    def connect_sock():
        """
        Keyword to create socket connection between UPTT to UE-NAS-Stack
        :return: Socket
        """
        config_path = Path(Path(__file__).parent.parent) / 'Resources' / 'config.ini'
        config = configparser.ConfigParser()
        config.read(config_path)
        ue_nas_section = config['UE-NAS']
        ue_nas_ip = ue_nas_section.get('ServerIP')
        ue_nas_port = int(ue_nas_section.get('ServerPort', '9999'))

        nas_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        nas_socket.connect((ue_nas_ip, ue_nas_port))
        return nas_socket
