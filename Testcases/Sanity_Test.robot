*** Variables ***
${LIBRARIES}    ../Library
${RESOURCES}    ../Resources


*** Settings ***
Documentation     Sanity test suite for UE NAS Stack.

Library           ${LIBRARIES}/UeNasLib.py
Variables         ${RESOURCES}/vars.py

Suite Setup  Connect ue nas socket
Suite Teardown  Disconnect ue nas socket

*** Test Cases ***
Sending registration message
    Send registration request    ${initial_reg_msg}

#Sending de-registration message
#    Send de registration request    ${Reg Body}



